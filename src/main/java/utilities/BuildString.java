package utilities;

public class BuildString {

    /**
     *  Builds a string with all the items in the options array.
     *  Each item is on a new line and has an index in front of it.
     *  The index starts at 1 and increase by one for each line.
     */
    public static String buildOptionsWithIndexes(String[] options) {
        int i = 1;
        StringBuilder query = new StringBuilder();
        for (String option : options) {
            query.append(i++).append(": ").append(option).append("\n");
        }
        return query.toString();
    }
}
