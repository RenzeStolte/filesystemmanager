package utilities;

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;

public class Writer {
    /**
     * Adds text to the specified fileName. Creates a new file if fileName doesn't exist.
     */
    public static void writeToFile(String text, String fileName) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(fileName, true);
            fileWriter.write(text + "\n");
        }
        catch (IOException e) {
            System.out.println("Could not write to file: " + e);
        }
        finally {
            try {
                // Close the writer only if it exists.
                if(fileWriter != null) {
                    fileWriter.close();
                }
            } catch (IOException e) {
                System.out.println("Error while trying to close writer: " + e);
            }
        }
    }
}
