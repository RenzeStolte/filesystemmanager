package utilities;

import java.util.Scanner;
import java.util.NoSuchElementException;

/*
 * UserInput has functions related to asking and taking input from the user.
 */
public class InputHandler {

	/**
	 * Lets the user choose from some options. User can choose by select 1 up to maxChoices.
	 * Query needs to provide directions for the user.
	 */
	public static int chooseFromOptions(String query, int maxChoices) {
		boolean validInput = false;
		int userInput = 0;
		while(!validInput) {
			userInput = InputHandler.getIntFromUser(query) - 1;
			if(userInput >= maxChoices || userInput < 0) {
				System.out.println("That is not a valid option, please select again.");
			} else {
				validInput = true;
			}
		}
		return userInput;
	}

	/**	
	 * Keeps asking the provided query until the user has given a valid integer.
	 */	
	public static int getIntFromUser(String query) {
		int userInt = -1;
		Scanner scanner = new Scanner(System.in);
		boolean noValidInput = true;

		while(noValidInput) {
			try {
				OutputHandler.printString(query);
				userInt = scanner.nextInt();
				noValidInput = false;
			} catch (NoSuchElementException e) {
				OutputHandler.printString("Please enter a valid integer.");
				scanner.nextLine();
			}
		}
		return userInt;
	}
	
	/**
	 * Keeps asking the provided query until the user has responded with a string.
	 */
	public static String getStringFromUser(String query) {
		String userResponse = "";
		Scanner scanner = new Scanner(System.in);
		boolean noValidInput = true;
		while(noValidInput) {
			try {
				OutputHandler.printString(query);
				userResponse = scanner.next();
				noValidInput = false;
			} catch (NoSuchElementException e) {
				System.out.println("Please enter a valid response.");
				scanner.nextLine();
			}
		}
		
		return userResponse;
	}
}