package utilities;

import java.io.*;
import java.util.*;


public class TextReader{

    private static boolean hasBuiltArray;
    private static String currentSortedFile;
    private static ArrayList<String> sortedText;

    /**
     * Returns the number of lines in the given file.
     */
    public static int GetNumberOfLinesInFile(String fileName) {
        BufferedReader bufferedReader = null;
        int lineCount = 0;
        try {
            bufferedReader = new BufferedReader(new FileReader("src/main/resources/" + fileName));
            String line = bufferedReader.readLine();
            while(line != null) {
                line = bufferedReader.readLine();
                lineCount++;
            }
        } catch(IOException e) {
            System.out.println("Error when trying to read the file " + fileName + ": " + e);
        } finally {
            try {
                if(bufferedReader != null)
                    bufferedReader.close();
            } catch (IOException e) {
                System.out.println("Error when trying to close the Buffered Reader: " + e);
            }
        }

        return lineCount;
    }

    /**
     * Returns how many times a specified word is present in a specified file.
     * Ignores lower or upper cases. 
     */
    public static int NumberOfWordInText(String fileName, String word) {
        word = word.toLowerCase();
        Scanner scanner = null;
        int nOfWord = 0;


        // Try to use the scanner
        try {
            scanner = new Scanner(new File(ReadResources.getFile(fileName).toString()));
            scanner.useDelimiter(" |\\n|\\r"); // Uses either a new line or an empty space as a delimiter.

            while(scanner.hasNext()) {
                String rawWord = scanner.next();
                String processedWord = rawWord.toLowerCase().replaceAll("[.,!?]", "");;
                if(processedWord.equals(word)) {
                    nOfWord++;
                }
            }
        }

        // Catch any exception
        catch (IOException e) {
            System.out.println("Error when trying to open the Scanner: " + e);
        }

        // Try to close the scanner
        finally {
            try {
                if(scanner != null) {
                    scanner.close();
                }
            } catch (Exception e) {
                System.out.println("Error when trying to close the Scanner: " + e);
            }
        }
        return nOfWord;
    }

    /**
     * Returns how many times a specified word is present in a specified file.
     * Ignores lower or upper cases.
     * If run for the first time with the currently selected .txt file, first makes a list
     * with all the words in the file. Then the list is sorted, and searched through using
     * binary search. Any subsequent searches will only need to use binary search.
     */
    public static int SmartNumberOfWordInText(String fileName, String word) {
        word = word.toLowerCase();
        if(hasBuiltArray && fileName.equals(currentSortedFile)) {
            return binarySearch(word);
        }

        // Builds an array with all the words from the text and then sorts it.
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(ReadResources.getFile(fileName).toString()));
            scanner.useDelimiter(" |\\n|\\r");
            sortedText = new ArrayList<String>();
            while(scanner.hasNext()) {
                String scannedWord = scanner.next().toLowerCase().replaceAll("[.,!?]", "");
                if(!scannedWord.equals("")) // Don't add the word if it is blank
                    sortedText.add(scannedWord);
            }
            // Sort the array and then search through it with binary search.
            sortedText.sort(null);
            hasBuiltArray = true;
            currentSortedFile = fileName;
            return binarySearch(word);
        }
        catch (IOException e) {
            OutputHandler.printString("Error when trying to open the Scanner: " + e);
        }
        finally {
            // Try to close the scanner
            try {
                if(scanner != null) {
                    scanner.close();
                }
            }
            catch (Exception e) {
                System.out.println("Error when trying to close the Scanner: " + e);
            }
        }
        return -1;
    }

    /**
     * Searches through the sortedText array fast.
     * Returns the number of times it encounters the word.
     */
    private static int binarySearch(String word) {
        // Indexes used to keep track of where we can search between:
        int min = 0;
        int max = sortedText.size();

        while(true) {
            // If max is less or equal to min, the words isn't in the array.
            if(max <= min) {
                return 0;
            }

            // Start at the middle of the currently non-eliminated indexes.
            int index = (int) Math.floor((min + max) / 2);

            // If the string at index is the same as word,
            // also check for the string on the right and left side of the index. Then return.
            if(sortedText.get(index).equals(word)) {
                int nOfWord = 1;
                nOfWord += checkRight(index + 1, word);
                nOfWord += checkLeft(index - 1, word);
                return nOfWord;
            } else {
                if (sortedText.get(index).compareTo(word) > 0) {
                    max = index;
                } else {
                    min = index + 1;
                }
            }
        }
    }

    /**
     * Recursively check if the string to the right of the index is the same as userWord.
     * Stops if the string at index is not the same.
     */
    private static int checkRight(int index, String userWord) {
        if(index < sortedText.size() && sortedText.get(index).equals(userWord)) {
            return 1 + checkRight(index + 1, userWord);
        } else {
            return 0;
        }
    }

    /**
     * Recursively check if the string to the left of the index is the same as userWord.
     * Stops if the string at index is not the same.
     */
    private static int checkLeft(int index, String userWord) {
        if(index >= 0 && sortedText.get(index).equals(userWord)) {
            return 1 + checkLeft(index - 1, userWord);
        } else {
            return 0;
        }
    }
}
