package utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {
    private long startTime;
    private long endTime;
    private boolean startedLog;
    private String logName;

    public Logger(String logName) {
        this.logName = logName;
    }

    /**
     * Saves the current time to keep track of how long an operation took.
     * Must be run before endLog.
     */
    public void startLog() {
        startedLog = true;
        startTime = System.currentTimeMillis();
    }

    /**
     * Returns a string with text containing information about the operation: current time and
     * time it took to complete operation.
     * Must be run after startLog.
     */
    public void stopLog(String operationName) {
        if(startedLog) {
            endTime = System.currentTimeMillis();
            startedLog = false;

            // Creates the message we want to display and sends it to be added to the log.
            String logMessage = String.format("%s: %s Took %d milliseconds ",
                    getCurrentTime(), operationName, (endTime - startTime));
            Writer.writeToFile(logMessage, logName);
        } else {
            OutputHandler.printString("Cannot end logging: Never started!");
        }
    }

    /**
     *  Adds a message in the log file, noting when a new session was started.
     */
    public void startSession() {
        String logMessage = String.format("%s. Started a session.", getCurrentTime());
        Writer.writeToFile(logMessage, logName);
    }

    /**
     *  Adds a message in the log file, noting when a session was ended.
     */
    public void stopSession() {
        String logMessage = String.format("%s. Stopped a session.", getCurrentTime());
        Writer.writeToFile(logMessage, logName);
    }

    /**
     * Adds a message in the log file, labeling as a problem.
     */
    public void reportProblem(String descriptionOfProblem) {
        String logMessage = String.format("%s. Encountered a problem: %s", getCurrentTime(), descriptionOfProblem);
        Writer.writeToFile(logMessage, logName);
    }

    /**
     * Returns a string with the current time in the format: yyyy/MM/dd HH:mm:ss
     */
    private String getCurrentTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }
}
