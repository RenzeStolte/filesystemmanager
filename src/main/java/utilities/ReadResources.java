package utilities;
import java.io.File;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;

public class ReadResources {

    private static String pathLocation = "src/main/resources/";

    /**
     * Returns a File of the given name. Only looks for files in src/main/resources
     */
    public static File getFile(String name) {
        File f = new File(pathLocation + name);
        return f;
    }

    /**
     * Returns all the file names and their extensions of the files located at scr/main/resources.
     */
    public static String[] getFileNames() {
        File f = new File(pathLocation);
        String[] fileNames = f.list();
        return fileNames;
    }

    /**
     * Returns a list of all files with the given extension.
     */
    public static String[] getFileNamesByExtension(String extension) {
        String[] allFileNames = getFileNames();
        List<String> fileNamesList = new ArrayList<String>();

        // Gets all the file names with the correct name into a list
        for (String fileName : allFileNames) {
            if(getExtensionOfFileName(fileName).equals(extension)){
                fileNamesList.add(fileName);
            }
        }

        // Gets all the file names with the correct name into an array
        return fileNamesList.toArray(new String[0]);
    }

    /**
     * Returns all the unique extensions located at src/main/resources
     */
    public static String[] getUniqueExtensions() {
        String[] fileNames = getFileNames();
        Set<String> extensionsSet = new HashSet<String>();

        // Gets all the extensions into set
        for (String fileName : fileNames) {
            extensionsSet.add(getExtensionOfFileName(fileName));
        }

        // Gets all the extensions into the array
        String[] extensionsArray = new String[extensionsSet.size()];
        int i = 0;
        for(String extension : extensionsSet) {
            extensionsArray[i++] = extension;
        }

        return extensionsArray;
    }

    /**
     * Returns the extensions of the given file name. Returns an empty string if there is no extension.
     */ 
    private static String getExtensionOfFileName(String fileName) {
        if(fileName.contains(".")) {
            return fileName.substring(fileName.lastIndexOf("."));
        } else {
            return "";
        }
    }
}
