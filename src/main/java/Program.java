import controllers.*;
import utilities.InputHandler;
import utilities.Logger;
import utilities.OutputHandler;

public class Program {
	// The location and name of the log file that will be created.
	static String logName = "Log.txt";

	public static void main(String[] args) {
		Logger logger = new Logger(logName);
		logger.startSession();
		UserChoices.getChoiceFromUserAndRunMethods(logger);
		logger.stopSession();
	}
	



}
