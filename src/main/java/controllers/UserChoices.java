package controllers;

import utilities.InputHandler;
import utilities.Logger;
import utilities.OutputHandler;

public class UserChoices {

    /**
     * Displays actions the user can take with corresponding indexes.
     * The user can input an index, after which the correct action will be taken.
     */
    public static void getChoiceFromUserAndRunMethods(Logger logger) {
        boolean runningProgram = true;
        while(runningProgram) {
            OutputHandler.printString("Please input a number to select one of the following options:");
            String query = "1 - Show the names of all the files present in the system.\n";
            query += "2 - Show files by extension.\n";
            query += "3 - View options for .txt files.\n";
            query += "4 - Exit this program.\n";

            int userInput = InputHandler.getIntFromUser(query);
            switch (userInput) {
                case 1 -> DisplayFiles.DisplayFileNames(logger);
                case 2 -> DisplayFiles.DisplayFileByExtension(logger);
                case 3 -> {

                    TxtMethods.TxtOptions(logger);
                }
                case 4 -> {
                    OutputHandler.printString("The program is now ending...");
                    runningProgram = false;
                }
                default -> OutputHandler.printString("That is not a valid option, please choose an other option.");
            }
        }
    }
}
