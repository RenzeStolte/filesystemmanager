package controllers;

import utilities.*;

import java.io.File;

public class TxtMethods {

    private static String currentTxt;

    /**
     * Gives the user options for .txt files, and executes their orders.
     */
    public static void TxtOptions(Logger logger) {
        String[] txtFiles = ReadResources.getFileNamesByExtension(".txt");
        if(txtFiles.length == 0) {
            OutputHandler.printString("There are no .txt files!");
            return;
        }
        currentTxt = txtFiles[0];

        String query = "1 - Change selected .txt file.\n";
        query += "2 - Show the size of the file\n";
        query += "3 - Show the number of lines in the file\n";
        query += "4 - Search for a specific word in the file\n";
        query += "5 - Use smart search to search for a specific word in the file\n";
        query += "8 - Return to the start screen\n";
        boolean showScreen = true;
        while(showScreen) {
            OutputHandler.printString("Currently selected file: " + currentTxt + ". What do you want to do?\n");
            int userInput = InputHandler.getIntFromUser(query);
            switch (userInput) {
                case 1 -> selectTxt();
                case 2 -> GetFileSize(logger);
                case 3 -> GetNumberOfLines(logger);
                case 4 -> SearchForWord(logger);
                case 5 -> SmartSearchForWord(logger);
                case 8 -> {
                    OutputHandler.printString("Returning to the main menu screen.");
                    showScreen = false;
                }
                default -> OutputHandler.printString("That is not a valid option, please choose an other option.");
            }
        }
    }

    /**
     *
     */
    private static void selectTxt(){
        OutputHandler.printString("Choose which .txt file you want to select.");
        String[] allTxtFiles = ReadResources.getFileNamesByExtension(".txt");
        String query = BuildString.buildOptionsWithIndexes(allTxtFiles);
        int userInput = InputHandler.chooseFromOptions(query, allTxtFiles.length);
        currentTxt = allTxtFiles[userInput];
        OutputHandler.printString("Change to " + currentTxt + ".");
    }

    /**
     * Finds the size of the text and outputs it to the user.
     */
    private static void GetFileSize(Logger logger) {
        logger.startLog();
        File file = ReadResources.getFile(currentTxt);
        long fileSize = file.length();
        OutputHandler.printString("Size of " + currentTxt + ": " + fileSize + " bytes.");
        logger.stopLog("Found the size of " + currentTxt + ". It has " + fileSize + " bytes.");
    }

    /**
     * Finds the number of lines in the text and outputs it to the user.
     */
    private static void GetNumberOfLines(Logger logger) {
        logger.startLog();
        long fileSize = TextReader.GetNumberOfLinesInFile(currentTxt);
        OutputHandler.printString("Number of lines in " + currentTxt + ": " + fileSize + " lines.");
        logger.stopLog("Found the number of lines in " + currentTxt
                + ". It has " + fileSize + " lines.");
    }

    /**
     * Asks the user for a word, then counts how many times that word is present in the text.
     * The result is output to the user.
     */
    private static void SearchForWord(Logger logger) {
        String query = "Please insert the word that you would like to search for: ";
        String userWord = InputHandler.getStringFromUser(query);

        logger.startLog();
        int nOfWord = TextReader.NumberOfWordInText(currentTxt, userWord);
        OutputHandler.printString("There are "+ nOfWord + " cases of " + userWord + " in " + currentTxt + ".");
        logger.stopLog("The term " + userWord + " was found " + nOfWord + " times.");
    }

    /*
     * Asks the user for a word, then counts how many times that word is present in the text.
     * Uses a smarter search system. The result is output to the user.
     */
    private static void SmartSearchForWord(Logger logger) {
        String query = "Please insert the word that you would like to search for: ";
        String userWord = InputHandler.getStringFromUser(query);

        logger.startLog();
        int nOfWord = TextReader.SmartNumberOfWordInText(currentTxt, userWord);

        OutputHandler.printString("There are "+ nOfWord + " cases of " + userWord + " in the text.");
        logger.stopLog("Used smart search: The term " + userWord + " was found " + nOfWord + " times.");
    }
}

