package controllers;

import utilities.*;

public class DisplayFiles {

    /**
     * Displays the name of all the files found in the resource folder
     */
    public static void DisplayFileNames(Logger logger) {
        logger.startLog();
        String[] fileNames = ReadResources.getFileNames();
        if(fileNames == null) {
            OutputHandler.printString("Could not find any files! Do you have a resource folder?");
            logger.stopLog("Could not find any files to display.");
        } else {
            OutputHandler.printString("These files are currently found in the resource folder: ");
            OutputHandler.printArray(fileNames);
            logger.stopLog("Displayed the file names.");
        }
    }

    /**
     * First lets the user choose from all the extensions found in the resource folder,
     * then shows all files that are of that extension.
     */
    public static void DisplayFileByExtension(Logger logger) {
        // Gets all unique extensions and logs it.
        logger.startLog();
        String[] extensions = ReadResources.getUniqueExtensions();
        logger.stopLog("Found " + extensions.length + " unique extensions.");

        // Displays then gets which extension the user wants
        OutputHandler.printString("Press the corresponding number for the extension you want to display:");
        String query = BuildString.buildOptionsWithIndexes(extensions);
        int userInput = InputHandler.chooseFromOptions(query, extensions.length);

        // Prints all files with chosen extension and logs.
        logger.startLog();
        String[] filesWithGiveExtension = ReadResources.getFileNamesByExtension(extensions[userInput]);
        OutputHandler.printArray(filesWithGiveExtension);
        logger.stopLog("Displayed all files of type " + extensions[userInput]
                + ". Found " + filesWithGiveExtension.length + ".");
    }
}
