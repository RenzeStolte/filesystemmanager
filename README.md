# File System Manager

# Overview
This program keeps track of different files located in the resources folder. The console will ask the user about different actions they can take to get information about those files. All actions by the user will be tracked in a Log.txt file located in the out folder. The log-file will also display how long each action takes, and when it was taken.

## Available actions
### File names
- Shows the names of all files in the resources folder
### File names by extension
- Lets the user choose between all the extensions present in the resources folder. When the user selects an extension, it will show all the corresponding files.
### Options regarding .txt files:
### Select .txt file
- Select a .txt file from the resources folder. Any other actions will use this .txt file.
### Length of the .txt
- Displays the length of the currently selected .txt file.
### Lines in .txt
- Counts and displays the number of lines in the currently selected .txt file.
### Search for a word in the .txt
- Searches through the entirity of the .txt, displaying how many times this word was encountered.
### Search "smartly" for a word in the .txt
- First creates a list of all the words from the .txt file and then sorts it. Finally, uses binary search to go through the list to find how many times the word is present. If the list already exists for the currently selected .txt file, it will instead go straight to the binary search part.
- The initial search will take a bit longer than the other search, but subsequent searches will be dramatically faster.
